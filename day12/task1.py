import numpy as np
import sys
from pprint import pprint
from queue import PriorityQueue
from collections import defaultdict
class Graph:

    def __init__(self):
        self.graph = defaultdict(list)

    def add_node(self,node):
        self.graph[node] = set()

    def add_neighbor(self,node,neighbor, dual=True):
        #neighbor should be (key, cost_off neighbor)
        self.graph[node].add(neighbor)
        if dual:
            self.graph[neighbor[0]].add((node,neighbor[1]))
    
    def get_neighbors(self,node):
        return self.graph[node]


myGraph = Graph()
mountains = dict()

lowlist=[]
with open(sys.argv[1]) as f:
    for y,line in enumerate(f.readlines()):
        for x,mountain in enumerate(line.strip()):
            if mountain == "S":
                start = (x,y)
                mountain = "s"
            if mountain == "E":
                end = (x,y)
                mountain = "z"
            if mountain == 'a':
                lowlist.append((x,y))
            mountains[x,y] = mountain

distances = {}
for mountain in (mountains.keys()):
    distances[mountain]=1e9
    myGraph.add_node(mountain)
    for offset in ((-1,0),(1,0),(0,1),(0,-1)):
        neighbor = (mountain[0]+offset[0],mountain[1]+offset[1])
        if neighbor in mountains:
            if ((ord(mountains[neighbor]) - ord(mountains[mountain])) >= -1):
                cost = 1
                myGraph.add_neighbor(mountain,(neighbor,cost),dual=False)
            
prioqueue = PriorityQueue()
prioqueue.put((0,end))
while not prioqueue.empty():
    next_node = prioqueue.get()
    if next_node[0]>distances[next_node[1]]:
        continue
    for neighbor in myGraph.get_neighbors(next_node[1]):
        distance = next_node[0] + neighbor[1]
        if distance<distances[neighbor[0]]:
            distances[neighbor[0]] = distance
            prioqueue.put((distance,neighbor[0]))
        else:
            pass

print(f" task1 {distances[start]}")
print(f" task2 {min([distances[i] for i in lowlist])}")