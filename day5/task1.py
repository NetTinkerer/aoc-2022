import re
import sys
from pprint import pprint
from collections import defaultdict

with open(sys.argv[1]) as f:
    crates, actions = f.read().split('\n\n')
stacks = defaultdict(list)
crates = crates.splitlines()
#remove index not needed
crates.pop()
for crate in crates:
    for i in range(len(crate)//4+1):
        if not crate[i*4+1] == " ":
            stacks[i+1].append(crate[i*4+1])

for i in stacks.keys():
    stacks[i].reverse()

for action in actions.splitlines():
    count,src,dest = map(int, re.findall("\d+", action))
    move = stacks[src][-1*count:]
    del stacks[src][-1*count:]
    if int(sys.argv[2]) == 9000:
        move.reverse() 
    stacks[dest] += move

print("".join([stacks[i][-1] for i in sorted(stacks.keys())]))
