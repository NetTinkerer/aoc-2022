import re
import sys
from pprint import pprint
games = []
with open(sys.argv[1]) as f:
    for line in f:
        rps = re.search(r"^(\w)\s(\w)", line)
        games.append((rps.group(1),rps.group(2)))

score_table = {
       ('A','X'):1+3,
       ('A','Y'):2+6,
       ('A','Z'):3+0,
       ('B','X'):1+0,
       ('B','Y'):2+3,
       ('B','Z'):3+6,
       ('C','X'):1+6,
       ('C','Y'):2+0,
       ('C','Z'):3+3
       }
result_table = {
       ('A','X'):'Z',
       ('A','Y'):'X',
       ('A','Z'):'Y',
       ('B','X'):'X',
       ('B','Y'):'Y',
       ('B','Z'):'Z',
       ('C','X'):'Y',
       ('C','Y'):'Z',
       ('C','Z'):'X'
       }
#pprint(games)

print(sum([score_table[game] for game in games]))
print(sum([score_table[game[0],result_table[game]] for game in games]))