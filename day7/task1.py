import sys
from pprint import pprint
import re
class Folder:

    def __init__(self,name):
        self.name = name
        self.files = []
        self.children = []
        self.size = -1
    def add_file(self,filename,size):
        self.files.append((filename,int(size)))
    def add_child(self,child):
        self.children.append(child)
    def show_files(self):
        return self.files
    def show_children(self):
        return self.children
    def __repr__(self):
        return self.name

def cfn(folder):
    # simple function to create a path name based on a list of paths
    return "/".join(folder)

cur_folder= [] 
folders = {'/':Folder('/')}
with open(sys.argv[1]) as f:
    for line in f.read().splitlines():
        cd = re.search(r"^\$\scd\s(.*)",line)
        filename = re.search(r"^(\d+)\s(.*)",line)
        dirname = re.search(r"^dir\s(.*)",line)
        if cd:
            #print(cd.group(1))
            if cd.group(1) == '..':
                cur_folder.pop()
            elif cd.group(1) == '/':
                cur_folder = ['/']
            else:
                cur_folder.append(cd.group(1))
                folders[cfn(cur_folder)]=Folder(cfn(cur_folder))
        if filename:
           # print("adding file {} to folder {}".format(filename.group(2),cfn(cur_folder)))
            folders[cfn(cur_folder)].add_file(filename.group(2),filename.group(1))
        if dirname:
           # print("adding dir {} to {}".format(dirname.group(1), cfn(cur_folder)))
            folders[cfn(cur_folder)].add_child(cfn(cur_folder + [dirname.group(1)]))
        
def get_size(folder):
    return sum([x[1] for x in folders[folder].show_files()]) + sum([get_size(child) for child in folders[folder].show_children()])

print("Parsing Done")

total_small = 0
free_folders = []
disk = 70000000
free = disk - get_size("/")
required = 30000000
to_free = required - free
print("to free",to_free)
for folder in folders:
    folder_size = get_size(folder)
    if folder_size<100000:
        total_small += folder_size
    if folder_size>=to_free:
        free_folders.append(folder_size)

print("part1",total_small)    
print("part2",sorted(free_folders)[0])