import re
import sys
from collections import deque 
class Monkey:
    def __init__(self,items,operation,test_div,t,f):
        self.items = deque(items)
        self.operation = operation
        self.test_div = test_div
        self.t = t
        self.f = f
        self.throws = 0
        self.inspects = 0
    def show_items(self):
        return self.items
    def throw_items(self):
        throw = len(self.items)
        throwlist = list()
        wl = 3
        for i in range(throw):
            old = self.items.popleft()
          #  print(f"throwing item {old}")
          #  print(self.operation)
            new = eval(self.operation)
          #  print(f" new {new//wl} dividing by {self.test_div}")
          #  print((new//wl)%self.test_div)
            new_monkey = self.t if not new//wl%self.test_div else self.f
          #  print(f"throwing {new//wl} to {new_monkey}")
            throwlist.append((new//wl,new_monkey))
            self.inspects += 1
        return throwlist
    def add_item(self, item):
        self.items.append(item)
    def get_inspects(self):
        return self.inspects
monkeys=dict()
monkeystuff=list()
with open(sys.argv[1]) as f:
    for line in f.readlines():
        #print(f"line {line}")
        monkey = re.search(r"Monkey (\d+):\n",line)
        if monkey:
            print("monkey")
            monkeystuff.append(monkey.group(1))
        items = re.search(r"  Starting items: (.*)",line)
        if items:
             monkeystuff.append(items.group(1))
        operation = re.search(r"  Operation: new = (.*)\n",line)
        if operation:
            monkeystuff.append(operation.group(1))
        test = re.search(r"  Test: divisible by (\d+)\n",line)
        if test:
            monkeystuff.append(test.group(1))
        t = re.search(r"    If true:.*monkey (\d+)\n",line)
        if t:
            monkeystuff.append(t.group(1))
        f = re.search(r"    If false:.*monkey (\d+)\n",line)
        if f:
            monkeystuff.append(f.group(1))
        
        if line.strip() == "":
            print("monkey done")
            print(monkeystuff)
            monkeys[int(monkeystuff[0])] = Monkey(list(map(int,monkeystuff[1].split(","))),
                                             monkeystuff[2],
                                             int(monkeystuff[3]),
                                             int(monkeystuff[4]),
                                             int(monkeystuff[5]))
            monkeystuff = []
                                        
print(monkeys.keys())     
for _ in range(20):                                        
    for monkey in monkeys.keys():
        #print(type(monkey))
        #print(f"monkey {monkey}")
        items = monkeys[monkey].show_items()
        for throw in monkeys[monkey].throw_items():
        #    print(throw)
            monkeys[throw[1]].add_item(throw[0])
    
inspects = list()
for monkey in monkeys.keys():
    print(monkey,monkeys[monkey].show_items(),monkeys[monkey].get_inspects())
    inspects.append(monkeys[monkey].get_inspects())

inspects.sort()
print(inspects[-2]*inspects[-1])



