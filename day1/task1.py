import re
from pprint import pprint
import sys
elves = [[]]
elf_index = 0
with open(sys.argv[1]) as f:
    for line in f:
        calories = re.search(r"^(\d+)", line)
        if calories:
            elves[elf_index].append(int(calories.group(1)))
        else:
            elves.append([])
            elf_index += 1
            
data = sorted([ sum(elf) for elf in elves ])
print("output")
print(data[-1])
print(sum(data[-3:]))

