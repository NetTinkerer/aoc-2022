import sys
from pprint import pprint
import re

cycle = 1
signal = {cycle: 1}
actions=list()
with open(sys.argv[1]) as f:
    for line in f.readlines():
        if line.startswith('noop'):
            cycle += 1
            signal[cycle] = signal[cycle-1]
        else: 
            cycle += 2
            signal[cycle] = signal[cycle-2] + int(line.split(" ")[1].strip())

spritepos={}
for i in range(1,241):
    try:
        spritepos[i] = [signal[i]-1,signal[i],signal[i]+1]
    except KeyError:
        spritepos[i] = [signal[i-1]-1,signal[i-1],signal[i-1]+1]

print(sum([spritepos[i][1]*i for i in range(20,221,40)]))
output=""

for i in range(0,240):
    output += chr(9608) if i%40 in spritepos[i+1] else " "

#print(output)
for i in range(0,6):
    print(output[i*40:40*(i+1)])