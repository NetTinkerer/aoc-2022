import numpy as np
import sys
from pprint import pprint

with open(sys.argv[1]) as f:
    trees = np.array([[x for x in y.strip()] for y in f.readlines()])

visible_count=0
scores = []

for x in range(1,trees.shape[0]-1):
    for y in range(1,trees.shape[1]-1):
        view_score=1
        visible = False
        tree_height = trees[x,y]
        #right
        right = trees[x,y+1:]
        #left
        left = trees[x,:y][::-1]
        #bottom
        bottom = trees[x+1:,y]
        #top
        top = trees[:x,y][::-1]
        for view in [left, right, top, bottom]:
            #print(view)
            for i,tree in enumerate(view):
                if tree >= tree_height:
                    view_score *= (i+1)
                    break
                #end of forrest
                if i == len(view)-1:
                    view_score *= (i+1)
                    visible = True     
        if visible:
            visible_count +=1
        scores.append(view_score)

print(f"part1: {visible_count+2*trees.shape[0]+2*trees.shape[1]-4}")
print(f"part2: {max(scores)}")        