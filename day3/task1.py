import re
import sys
from pprint import pprint
from collections import defaultdict

duplicates = defaultdict(int)
groups = defaultdict(int)

with open(sys.argv[1]) as f:
    data=f.read().splitlines() 

def gearscore(letter):
       score = ord(letter.lower())-96
       if letter.isupper():
              score += 26
       return score

for sack in data:
       dup = next(iter((set(sack[:int(len(sack)/2)]).intersection(set(sack[int(len(sack)/2):])))))
       duplicates[dup] += 1

print(sum([ gearscore(i)*duplicates[i] for i in duplicates.keys()]))

for i in range(0,len(data),3):
       group_id = next(iter((set(data[i:i+3][0]).intersection(set(data[i:i+3][1]))).intersection(set(data[i:i+3][2]))))
       groups[group_id] += 1

print(sum([ gearscore(i)*groups[i] for i in groups.keys()]))