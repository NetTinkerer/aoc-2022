import sys
from pprint import pprint
import re

actions=list()
with open(sys.argv[1]) as f:
    for y in f.readlines():
        action = (y.strip().split(" "))
        actions.append((action[0],int(action[1])))
knots = [[0,0] for i in range(10)]
positions = [{(0,0)} for i in range(10)]
movement = {'R': (1,0),
            'L': (-1,0),
            'U': (0,1),
            'D': (0,-1)}
for action in actions:
    for count in range(action[1]):
        knots[0] = [knots[0][0]+movement[action[0]][0],knots[0][1]+movement[action[0]][1]]
        for j in range(1,10):
            taildiff = (knots[j-1][0]-knots[j][0])**2+(knots[j][1]-knots[j-1][1])**2
            if taildiff in [4,5,8]:
                for i in [0,1]:
                    if knots[j-1][i] != knots[j][i]:
                        if knots[j-1][i] > knots[j][i]:
                            knots[j][i] += 1
                        else:
                            knots[j][i] -= 1
                positions[j].add((knots[j][0],knots[j][1]))

print("final output")
print(len(positions[1]))
print(len(positions[9]))

