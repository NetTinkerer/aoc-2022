import sys
from collections import deque 

signal = deque([])
i = 0
limit = int(sys.argv[2])
with open(sys.argv[1]) as f:
    while i<limit:
        signal.append(f.read(1))
        i += 1
    while True:
        if len(set(signal))==limit:
            print(i)
            break
        else:
            signal.append(f.read(1))
            signal.popleft()
            i += 1
